# Node Chat
Code to provide custom chat feature. Identify users by their Facebook ID.

# Tech stack
Passport.js to authenticate users locally and with Facebook.

node.js express framework to provide MVC structure.

ejs for view.

socket.io for chat.


## Instructions for running
If you would like to download the code and try it for yourself:

1. Clone the repo: `git clone https://ramaswav@bitbucket.org/ramaswav/node-chat.git`

2. Install packages: `npm install`

3. You would need to install mogodb prior to this, and create an collection called passport. if this has been done no need to change this file. Else change to appropriate config.

5. Launch: `npm start`

6. Visit in your browser at: `http://localhost:8080` you will be presented with profile page with your facebook details, visit the chat tab and you can send chat messages

7. Testing: open a window with local login ( signup before) and hit the chat tab, open another incognito window with the app and login using your facebook credentials, hit the chat tab, start chatting !. 
   Your chat's should be visible in both the windows.

